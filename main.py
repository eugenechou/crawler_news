#encoding: utf-8
import crawlers.news_ltn as news_ltn
import crawlers.ettoday as ettoday
import crawlers.chinatimes as chinatimes
import crawlers.setn as setn
import crawlers.storm as storm
import crawlers.ttv as ttv
import crawlers.cna as cna

if __name__ == "__main__":
    news_ltn.run()
    ettoday.run()
    chinatimes.run()
    setn.run()
    storm.run()
    ttv.run()
    cna.run()

    print("finish all!")