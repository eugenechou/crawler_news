#encoding: utf-8
import requests
from bs4 import BeautifulSoup
import datetime

page_to_crawle = 60

def run():
    print("start crawling ettoday")

    file = open("ettoday.txt", "w", encoding="UTF-8")
    a = []

    for i in range(page_to_crawle):
        date = (datetime.datetime.now() - datetime.timedelta(days=i)).strftime("%Y-%m-%d")
        date_check = (datetime.datetime.now() - datetime.timedelta(days=i)).strftime("%Y/%m/%d")
        url = "https://www.ettoday.net/news/news-list-" + date + "-1.htm"
        resp = requests.get(url)
        soup = BeautifulSoup(resp.text, "html.parser")
        sel = soup.select("div.part_list_2 h3")

        for s in sel:
            if s.span.string.find(date_check) != -1:
                a.append("https://www.ettoday.net" + s.a["href"])

    for i in a:
        resp = requests.get(i)
        soup = BeautifulSoup(resp.text, "html.parser")
        title = soup.select("div.subject_article header h1.title")
        file.write(title[0].string + " url: " + i + "\n")

        content = soup.select("div.story")
        for s in content[0]:
            if s.name == "p" and s.string and not s.strong and not s.span:
                file.write(s.string + "\n")
        file.write("\n\n")

    file.close()
    print("finish ettoday, save result in ettodays.txt")

if __name__ == "__main__":
    run()