#encoding: utf-8
import requests
from bs4 import BeautifulSoup
import datetime

page_to_crawle = 100

def run():
    print("start crawling storm")

    file = open("storm.txt", "w", encoding="UTF-8")
    a = []

    for i in range(page_to_crawle):
        url = "https://www.storm.mg/category/118/" + str(i+1)

        resp = requests.get(url)
        soup = BeautifulSoup(resp.text, "html.parser")
        sel = soup.select("div.category_card div.card_inner_wrapper a.link_title")
        
        for s in sel:
            a.append(s["href"])

    for i in a:
        resp = requests.get(i)
        soup = BeautifulSoup(resp.text, "html.parser")
        title = soup.select("div.page_wrapper header#article_title_wrapper h1#article_title")
        file.write(title[0].string + " url: " + i + "\n")

        content = soup.select("div#CMS_wrapper")
        for s in content[0]:
            if (s.name == "p" or s.name == "h2") and s.string:
                file.write(s.string + "\n")
        file.write("\n\n")

    file.close()
    print("finish storm, save result in storm.txt")

if __name__ == "__main__":
    run()
