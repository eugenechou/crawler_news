#encoding: utf-8
import requests
from bs4 import BeautifulSoup
import datetime

page_to_crawle = 74

def run():
    print("start crawling setn")

    file = open("setn.txt", "w", encoding="UTF-8")
    a = []

    for i in range(page_to_crawle):
        url = "https://www.setn.com/ViewAll.aspx?PageGroupID=6&p=" + str(i+1)
        resp = requests.get(url)
        soup = BeautifulSoup(resp.text, "html.parser")
        sel = soup.select("div.col-sm-12 div h3.view-li-title a")

        for s in sel:
            a.append("https://www.setn.com" + s["href"])
    
    for i in a:
        resp = requests.get(i)
        soup = BeautifulSoup(resp.text, "html.parser")
        title = soup.select("div.col-lg-9 h1.news-title-3")
        file.write(title[0].string + " url: " + i + "\n")

        content = soup.select("div#Content1 p")
        for s in content:
            if str(s).find("style") == -1 and s.string:
                file.write(s.string + "\n");
        file.write("\n\n")

    file.close()
    print("finish setn, save result in setn.txt")

if __name__ == "__main__":
    run()