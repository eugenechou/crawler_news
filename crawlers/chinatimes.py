#encoding: utf-8
import requests
from bs4 import BeautifulSoup
import datetime

page_to_crawle = 10

def run():
    print("start crawling chinatimes")

    file = open("chinatimes.txt", "w", encoding="UTF-8")
    a = []

    for i in range(page_to_crawle):
        url = "https://www.chinatimes.com/realtimenews/260407?page=" + str(i+1)
        resp = requests.get(url)
        soup = BeautifulSoup(resp.text, "html.parser")
        sel = soup.select("div.col h3.title a")

        for s in sel:
            a.append("https://www.chinatimes.com" + s["href"] + "?chdtv")
    
    for i in a:
        resp = requests.get(i)
        soup = BeautifulSoup(resp.text, "html.parser")
        title = soup.select("div.column-wrapper header.article-header h1.article-title")
        file.write(title[0].string + " url: " + i + "\n")

        content = soup.select("div.article-body p")
        for s in content:
            if s.string:
                file.write(s.string + "\n")
        file.write("\n\n")

    file.close()
    print("finish chinatimes, save result in chinatimes.txt")

if __name__ == "__main__":
    run()