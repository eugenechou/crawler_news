#encoding: utf-8
import requests
from bs4 import BeautifulSoup
import datetime

page_to_crawle = 17

ACCOUNT = "EMAIL"
PASSWORD = "PASSWORD"
LOGIN_URL = "https://auth.appledaily.com/web/v6/apps/598aee773b729200504d1f31/login"

def run():
    print("start crawling apple")

    p = requests.Session()
    headers = {
        'authority': 'auth.appledaily.com',
        'method': 'POST',
        'path': '/web/v6/apps/598aee773b729200504d1f31/login',
        'scheme': 'https',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
        'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7,da;q=0.6',
        'cache-control': 'no-cache',
        'content-length': '65',
        'content-type': 'application/x-www-form-urlencoded',
        'cookie': '_ga=GA1.2.711424473.1547917273; _parsely_visitor={%22id%22:%22f0a2c9d7-dc3d-41ba-b254-dad6bdfc8e3f%22%2C%22session_count%22:1%2C%22last_session_ts%22:1547917273394}; __gads=ID=42df12173b14979c:T=1547917272:S=ALNI_MbaPTZ-DNT_5JnGNYOXaPZJ3rAweA; nxtu=1523895484.337CACBD3-0CDC-4E8D-89F6-8380DA68CC5C; omo.sid=s%3Ag5ZSgdDNgRElZI_VMoOVylc1o8Dl-PwG.eDIORS1mw7sSSRfhL%2FvVNDeTapGNTG0Q4%2FcjL3YSFaQ; LPVID=ZmMzlmYjU0MDA0OTlhNjA3; LPSID-49269227=TlYdIYYaQtGemR3wlfk3Dw; _fbp=fb.1.1558862163512.127921235; _gid=GA1.2.1615154969.1559030232; country=TW; articleValid=20190529155838; articleExpires=Wed%20May%2029%202019%2015%3A58%3A38%20GMT%2B0800%20(%E5%8F%B0%E5%8C%97%E6%A8%99%E6%BA%96%E6%99%82%E9%96%93); article=0; articleID=; OMO_LSTLGWAY=email; lang=zh_tw; isLogin=true; isLoggedIn=true; omoaccid=5cecec8aa7b47000349ef0b5; adisblk=N; _gat_UA-119261200-16=1',
        'origin': 'https://auth.appledaily.com',
        'pragma': 'no-cache',
        'referer': 'https://auth.appledaily.com/web/v6/apps/598aee773b729200504d1f31/login',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'
    }
    payload = {
        'email': ACCOUNT,
        'password': PASSWORD
    }
    resp = p.post(LOGIN_URL, data=payload, headers=headers)
    file = open("apple.txt", "w", encoding="UTF-8")
    a = []

    for i in range(page_to_crawle):
        url = "https://tw.news.appledaily.com/politics/realtime/" + str(i+1)
        resp = p.get(url)
        soup = BeautifulSoup(resp.text, "html.parser")
        sel = soup.select("div.abdominis ul.rtddd li.rtddt a")

        for s in sel:
            a.append("https://tw.news.appledaily.com" + s["href"])
    
    for i in a:
        resp = p.get(i)
        soup = BeautifulSoup(resp.text, "html.parser")
        title = soup.select("article.ndArticle_leftColumn hgroup h1")
        file.write(title[0].string + " url: " + i + "\n")
        
        content = soup.select("article.ndArticle_content div.ndArticle_margin p")
        for s in content[0]:
            if s.string:
                file.write(s.string + "\n")
        file.write("\n\n")

    file.close()
    print("finish apple, save result in apple.txt")

if __name__ == "__main__":
    run()