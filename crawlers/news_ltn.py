#encoding: utf-8
import requests
from bs4 import BeautifulSoup
import datetime

page_to_crawle = 60

def run():
    print("start crawling news_ltn")

    file = open("news_ltn.txt", "w", encoding="UTF-8")
    a = []

    for i in range(page_to_crawle):
        date = (datetime.datetime.now() - datetime.timedelta(days=i)).strftime("%Y%m%d")
        url = "https://news.ltn.com.tw/list/newspaper/politics/" + date

        resp = requests.get(url)
        soup = BeautifulSoup(resp.text, "html.parser")
        sel = soup.select("div.whitecon ul.list li a.tit")
        
        for s in sel:
            a.append("https://news.ltn.com.tw/" + s["href"])

    for i in a:
        resp = requests.get(i)
        soup = BeautifulSoup(resp.text, "html.parser")
        title = soup.select("div.whitecon h1")
        tit = ""
        k = 0
        for t in title[0]:
            if k == 1:
                tit = t
            k += 1
        file.write(tit + " url: " + i + "\n")

        content = soup.select("div.text")
        for i in content[0]:
            if (i.name == "p" or i.name == "h4") and i.string:
                file.write(i.string + "\n")
        file.write("\n\n")

    file.close()
    print("finish news_ltn, save result in news_ltn.txt")

if __name__ == "__main__":
    run()