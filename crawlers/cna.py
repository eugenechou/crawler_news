#encoding: utf-8
import requests
from bs4 import BeautifulSoup
import datetime
import json

page_to_crawle = 5

def run():
    print("start crawling cna")

    file = open("cna.txt", "w", encoding="UTF-8")
    a = []
    
    for i in range(page_to_crawle):
        url = "https://www.cna.com.tw/cna2018api/api/simplelist/categorycode/aipl/pageidx/" + str(i+1) + "/"
        resp = requests.get(url)
        myDict = json.loads(resp.text)
        for j in myDict["result"]["SimpleItems"]:
            a.append(j["PageUrl"])

    for i in a:
        resp = requests.get(i)
        soup = BeautifulSoup(resp.text, "html.parser")
        title = soup.select("div.centralContent h1")
        file.write(title[0].string + " url: " + i + "\n")
        content = soup.select("div.paragraph p")
        for s in content:
            if s.string:
                file.write(s.string + "\n")
        file.write("\n\n")
        
    file.close()
    print("finish cna, save result in cna.txt")

if __name__ == "__main__":
    run()