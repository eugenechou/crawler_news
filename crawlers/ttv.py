#encoding: utf-8
import requests
from bs4 import BeautifulSoup
import datetime

page_to_crawle = 76

def run():
    print("start crawling ttv")

    file = open("ttv.txt", "w", encoding="UTF-8")
    a = []

    for i in range(page_to_crawle):
        url = "https://www.ttv.com.tw/news/catlist.asp?page=" + str(i+1) + "&Cat=A&NewsDay="
        resp = requests.get(url)
        soup = BeautifulSoup(resp.text, "html.parser")
        sel = soup.select("div.panel div.panel-body ul.list_style_none li.newsText a")

        for s in sel:
            a.append("https://www.ttv.com.tw" + s["href"])

    for i in a:
        resp = requests.get(i)
        soup = BeautifulSoup(resp.text, "html.parser")
        title = soup.select("h1.title")
        file.write("url :" + i + "\n")
        for t in title[0]:
            if not t.name:
                file.write(str(t).encode('iso-8859-1').decode('utf8') + "\n")
        
        content = soup.select("div.br")
        for s in content[0]:
            if s.string and not s.name and s.string.find("VarTxtAD") == -1:
                file.write(s.string.encode('iso-8859-1').decode('utf8'))
        file.write("\n\n")
    file.close()
    print("finish ttv, save result in ttv.txt")

if __name__ == "__main__":
    run()
    